const withCSS = require('@zeit/next-css')
module.exports = withCSS({
    webpack: function (config, {dev}) {
        if(dev) {
          config.devtool = 'cheap-module-source-map'
        }
        config.module.rules.push({
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 100000,
              name: '[name].[ext]'
            }
          }
        })
        return config
      },
      exportPathMap: function () {
        return {
          '/': { page: '/' }
        }
      }
})