import React, { Component } from 'react'
import LoggedInNavbar from '../components/navbar_logged_in'
import Footer from '../components/footer'
import Navbar from '../components/navbar'
import TaskCategory from '../components/taskCategory'
import Members from '../components/members'

// export default (Page) => {

  class HomePageLayout extends Component {
    constructor (props) {
      super(props)
      this.state = {
          loginStatus: false
      }
      this.changeLoginStatus = this.changeLoginStatus.bind(this)
    }

    changeLoginStatus (status) {
        console.log('ssssss', status)
        this.setState({loginStatus: status})
    }

    componentDidMount () {
        console.log('============>>>>>>>>>>', localStorage.getItem('token'))
        let status = localStorage.getItem('token') ? true : false
        this.setState({loginStatus: status})
        this.adjustPositionOfBannerText()
        this.addSliderToSecondSection()
        this.addResponsesToNavbar()
    }

    adjustPositionOfBannerText() {
      console.log('adjustPositionOfBannerText')
      var marginLeft = $('.container').css('margin-left');
      $('.banner-text').css('left', marginLeft);
    }

    addSliderToSecondSection() {
      $('.info-slider .container').slick({
    
      });
      
      var sliderControl = $('<img src="static/arrow.png">');
      $('.slick-next, .slick-prev').append($(sliderControl));
    
      $('.slider-container').slick({
        centerMode: true,
        infinite: true,
        centerPadding: '90px',
        slidesToShow: 3,
        slidesToScroll : 1,
        autoplay: true,
        speed: 2000,
        responsive: [
        {
          breakpoint: 1300,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: false
          },
          breakpoint: 769,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false
          }
    
    
        }
        ]
      });
    
      var secondSliderControl = $('<img src="static/slider.png">');
      $('.slider-container .slick-next, .slider-container .slick-prev').append($(secondSliderControl));
    
    }

    addResponsesToNavbar() {
      $('.hamburger-menu').on('click',function(){
        console.log('hey');
        $(this).toggleClass('hamburger-menu-visible');
        $('.nav-container').toggleClass('nav-container-visible')
        $('.navbar-brand').toggleClass('navbar-brand-hidden');
        $('.main-action').toggleClass('main-action-visible');
      });
    }

    render () {
      return(<div>
          { this.state.loginStatus ? <LoggedInNavbar home={true}/>: <Navbar changeLoginStatus={this.changeLoginStatus}/> }
          <section className="main-banner">
            <img src="../static/banner-image.jpg" />
            <div className="banner-text container">
                <h2>Get your to-do list done</h2>
                <p>Over 2 million trusted people across Australia happy to help.</p>
                <a href="" onClick={e=> {
                  e.preventDefault()
                  $('.signup-bs-example-modal-lg').modal('toggle')
                }}>Get started now</a>
            </div>  
          </section>
          <Members/>
          <TaskCategory/>
          <div className="clearfix"></div>
          <Footer/>
        </div>)
    }
  }

  export default HomePageLayout
// }
