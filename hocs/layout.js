import React, { Component } from 'react'
import Navbarloggedin from '../components/navbar_logged_in'
import Footer from '../components/footer'
import Navbar from '../components/navbar'

class Layout extends Component {
  constructor(props) {
      super(props)
      this.state = {
          userId: null
      }
  }
  
  render () {

      return (
          <React.Fragment>
              <div className={`inside-page ${this.props.class}`}>
                <Navbarloggedin />
                {/* <Navbar /> */}
                {this.props.children}
              </div>
              <Footer/>
          </React.Fragment>
      )
  }
}

export default Layout