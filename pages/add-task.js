import React, { Component } from 'react'
import Layout from '../hocs/layout'
import AddTaskForm from '../components/tasker/addTaskForm'
import withData from '../lib/withData'


class AddTasker extends Component {
    state = {  }
    render() { 
        return ( 
            // <React.Fragment>
            //     <div className="inside-page">
            //         <Navbarloggedin />
                    <Layout>
                     <AddTaskForm />
                    </Layout>
            //     </div>
            // <Footer />
            // </React.Fragment>
        );
    }
}
 
// export default Addtasker;
export default withData(() => (<AddTasker/>));
