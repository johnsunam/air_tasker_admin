import React, {Component} from 'react'
import withData from '../lib/withData'
import HomePageLayout from '../hocs/homePageLayout'

class Test extends Component {
    static async getInitialProps(ctx) {
        const { req, res} = ctx;
        console.log('//////', req.query);
        return true;
      }
    render () {
        return <HomePageLayout/>
    }
}

export default withData(() => (<Test/>));
