import React, {Component} from 'react';
import Navbar from '../components/navbar';
import Footer from '../components/footer';
import withData from '../lib/withData'


class Index extends Component {
    static async getInitialProps(ctx) {
        const { req, res} = ctx;
        console.log('//////', req.query);
        return true;
      }
    render () {
        return <div>
    <Navbar/>
    <section className="main-banner">
        <img src="../static/banner-image.jpg" />
        <div className="banner-text container">
            <h2>Get your to-do list done</h2>
            <p>Over 2 million trusted people across Australia happy to help.</p>
            <a href="">Get started now</a>
        </div>  
    </section>

    <section className="info-slider">
        <div className="decorative-container"></div>
        <div className="container">
        {/* <Slider {...settings}> */}
            <div className="main-info-content">
                <div className="info-text col-md-6">
                    <h2>Contrary to popular belief, Lorem Ipsum.</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                    <div className="personal-info">
                        <img src="static/author.jpg" />
                        <span className="personal-info-text">
                            <span className="person-name">
                                Brandan Quick
                            </span>
                            <span className="position">
                                CEO and co-founder
                            </span>
                        </span>
                    </div>
                </div>
                <div className="video-container col-md-6">
                    <video controls>
                      <source src="static/mov_bbb.mp4" type="video/mp4" />
                      <source src="static/mov_bbb.ogg" type="video/ogg" />
                    </video>
                </div>
            </div>
             <div className="main-info-content">
                <div className="info-text col-md-6">
                    <h2>Contrary to popular belief, Lorem Ipsum.</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                    <div className="personal-info">
                        <img src="static/author.jpg"/>
                        <span className="personal-info-text">
                            <span className="person-name">
                                Brandan Quick
                            </span>
                            <span className="position">
                                CEO and co-founder
                            </span>
                        </span>
                    </div>
                </div>
                <div className="video-container col-md-6">
                    <video controls>
                      <source src="static/mov_bbb.mp4" type="video/mp4"/>
                      <source src="static/mov_bbb.ogg" type="video/ogg"/>
                    </video>
                </div>
            </div>
            {/* </Slider> */}
        </div>
    </section>

    <section className="offers-sliders col-md-12">
        <h2>Latest Projects</h2>
        <h4>Cats:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg"/>
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg"/>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg"/>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>

        <h4>For Breeding:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>

        <h4>Rats:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>

        <h4>Rabbits:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>
    </section>
    <div className="clearfix"></div>
    <Footer/>
    </div>
    }
}

export default withData(() => (<Index/>));
