import React, { Component } from 'react';
import Layout from '../hocs/layout'
import withData from '../lib/withData'


class Taskdetails extends Component {
    state = {  }
    render() { 
        return ( 
            <Layout>
                <div className="task-detail container">
                    <div className="task-listing-left col-md-4">
                        <span className="new-task-added">8 new tasks</span>
                        <div className="task-listing-wrapper">
                            <div className="task-info">
                                <h3>SE0 and google ranking</h3>
                                <p className="calendar">Wed 16, Jan</p>
                                <p className ="time">Anytime</p>
                                <p className="web">Remote</p>
                                
                            </div>
                            <div className="task-price-info">
                                <h3 >$1000</h3>
                                <div className="task-user-image">
                                    <img src="../static/user.png"/>
                                </div>
                            </div>
                            <div className="task-status">
                                <span className="status">OPEN</span><span className="number-of-offers">1 offer</span>
                            </div>
                        </div>
                        <div className="task-listing-wrapper task-listing-wrapper-completed">
                            <div className="task-info">
                                <h3>SE0 and google ranking</h3>
                                <p className="calendar">Wed 16, Jan</p>
                                <p className ="time">Anytime</p>
                                <p className="web">Remote</p>
                                
                            </div>
                            <div className="task-price-info">
                                <h3 >$1000</h3>
                                <div className="task-user-image">
                                    <img src="../static/user.png"/>
                                </div>
                            </div>
                            <div className="task-status">
                                <span className="status">OPEN</span><span className="number-of-offers">1 offer</span>
                            </div>
                        </div>
                        <div className="task-listing-wrapper">
                            <div className="task-info">
                                <h3>SE0 and google ranking</h3>
                                <p className="calendar">Wed 16, Jan</p>
                                <p className ="time">Anytime</p>
                                <p className="web">Remote</p>
                                
                            </div>
                            <div className="task-price-info">
                                <h3 >$1000</h3>
                                <div className="task-user-image">
                                    <img src="../static/user.png"/>
                                </div>
                            </div>
                            <div className="task-status">
                                <span className="status">OPEN</span><span className="number-of-offers">1 offer</span>
                            </div>
                        </div>
                        <div className="task-listing-wrapper">
                            <div className="task-info">
                                <h3>SE0 and google ranking</h3>
                                <p className="calendar">Wed 16, Jan</p>
                                <p className ="time">Anytime</p>
                                <p className="web">Remote</p>
                                
                            </div>
                            <div className="task-price-info">
                                <h3 >$1000</h3>
                                <div className="task-user-image">
                                    <img src="../static/user.png"/>
                                </div>
                            </div>
                            <div className="task-status">
                                <span className="status">OPEN</span><span className="number-of-offers">1 offer</span>
                            </div>
                        </div>
                    </div>
                    <div className = "main-task-detail col-md-8">
                    <div className="col-md-4 main-task-options">
                            <div className="main-task-value">
                                <p className="side-budget-title">Task Budget</p>
                                <h2>$1000</h2>
                                <span className="make-an-offer">Make An Offer</span>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="sel1">
                                    <option selected= "selected">More options</option>                          
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <div class="task-detail-media">
                             <p>Share</p>
                                <i class="fab fa-facebook-f"></i>
                                <i class="fab fa-twitter"></i>
                                <i class="fab fa-google-plus-g"></i>
                                <i class="fab fa-linkedin-in"></i>
                                <i class="fas fa-code"></i>
                            </div>
                            <p className="report-task"><i class="fas fa-flag"></i> Report this task</p>
                        </div>
                        <div className="col-md-8">
                            <div className="tag-wrapper">
                                <span>open</span>
                                <span>assigned</span>
                                <span>completed</span>
                            </div>
                            <h2 className="main-task-name">
                                Social Media and SEO management
                            </h2>
                            <div className="tasker-detail-wrapper">
                                <div className="tasker-detail">
                                    <img src="../static/user.png"/>
                                    <div className="tasker-name">
                                        <span className="posted-by">posted by</span>
                                        <span className="posted-name">Ratina P.</span>
                                    </div>
                                    <div className="posted-time"> 1 hour ago</div>
                                </div>
                            </div>
                            <div className="tasker-detail-wrapper">
                                <div className="tasker-detail another-detail">
                                    <img src="../static/location1.png"/>
                                    <div className="tasker-name">
                                        <span className="posted-by">location</span>
                                        <p>Remote</p>
                                    </div>
                                </div>
                            </div>
                            <div className="tasker-detail-wrapper">
                                <div className="tasker-detail another-detail">
                                    <img src="../static/calendar1.png"/>
                                    <div className="tasker-name">
                                        <span className="posted-by">due date</span>
                                        <p>Friday, 18th Jan 2019</p>
                                        <p>Anytime</p>
                                    </div>
                                </div>
                            </div>
                            <div className="main-detail-container">
                                <p className="detail-starter">Details</p>
                                <p>
                                Need someone with social media and SEO knowledge and experience to be able to get organic followers and growth for a boutique Realestate office based in Brisbane. Require someone approx. 2-3 hours per week.  
                                </p>
                                <p>Remotely.</p>
                                <p>Approx. 3 posts per week on instagram, FB, LinkedIn and twitter.</p>
                                <p>Also need advice on seo and improvements to make to website so I can boost traffic via website and social media</p>
                               
                                <div className="offer-starter">
                                    <p className="detail-starter">offers</p>
                                    <div className="offerer-details">
                                        <img src="../static/user.png"/>
                                        <div className="offerer-info">
                                            <p className="offerer-name">
                                                John Sunam
                                            </p>
                                            <ul className="offerer-rating">
                                                <li><img src="../static/active-rating.png"/></li>
                                                <li><img src="../static/active-rating.png"/></li>
                                                <li><img src="../static/active-rating.png"/></li>
                                                <li><img src="../static/active-rating.png"/></li>
                                                
                                            </ul>
                                            <span className="number-of-rating">(20)</span>
                                            <span className="completion-rate">100%</span>
                                            <span className="completion-rate-info-text">Completion rate</span>
                                        </div>
                                    </div>
                                    <div className ="offerer-comment">
                                        <div className="main-offerer-comment-wrapper">
                                            <p>Hi</p>
                                            <p>Offer includes 22% airtasker fee. My offer is for 2 hrs of work including Site analysis report Recommendations on overall seo and user experience Keywords analysis </p>
                                            <p>Thanks</p>
                                            <p>MUhammad</p>
                                        </div>
                                        <span className="time-counter">2 hours ago</span>
                                    </div>
                                    <div class="make-offer">
                                        <img src="../static/offer.png" />
                                        <span className="make-offer-button">Make an offer</span>                                    
                                    </div>
                                </div>
                                <div className="questions-starter">
                                    <p className="detail-starter">Questions  (1)</p>
                                    <p>Please don't share personal info – insurance won't apply to tasks not done through Company Name!</p>
                                    <div className="login-to-comment">
                                        <h2>To join the conversation</h2>
                                        <span className="make-offer-button">Join Company</span> or <span className="make-offer-button">Login</span>
                                    </div>
                                    <div className="offerer-details">
                                        <img src="../static/user.png"/>
                                        <div className="offerer-info">
                                            <p className="offerer-name">
                                                John Sunam
                                            </p>
                                        </div>
                                        <div className="main-comment-section">
                                            <p>Hi</p>
                                            <p>Yes. Ideally what I need is organic growth and I need someone to utilise hashtags to obtain this growth and presence. Same as fb. I have a boutique real estate office in Brisbane which I operate from home.</p>
                                            <span className="time-counter">2 minutes ago</span>
                                        </div>
                                    </div>

                                    <div className="offerer-details second-offerer-details">
                                        <img src="../static/user.png"/>
                                        <div className="offerer-info">
                                            <p className="offerer-name">
                                                John Sunam
                                            </p>
                                        </div>
                                        <div className="main-comment-section">
                                            <p>Hi</p>
                                            <p>Yes. Ideally what I need is organic growth and I need someone to utilise hashtags to obtain this growth and presence. Same as fb. I have a boutique real estate office in Brisbane which I operate from home.</p>
                                            <span className="time-counter">2 minutes ago</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </Layout>
         );
    }
}
 
export default withData(() => (<Taskdetails/>));
