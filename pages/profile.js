import React, { Component } from 'react'
import withData from '../lib/withData'
import Layout from '../hocs/layout'
import ProfileDetail from '../components/accounts/profileDetail'

class Profile extends Component {
  constructor (props) {
      super(props)
      this.state = {
          userId: null
      }
  }
  componentDidMount () {
    this.setState({userId: localStorage.getItem('userId')})
  }
  render () {
    return( <Layout>
        <section className="profile-page container">
        {this.state.userId ? <ProfileDetail id={this.state.userId}/>:<div>Loading....</div>}
        	
        	
        </section>
    </Layout>
    )
  }
}


export default withData(() => (<Profile/>));
