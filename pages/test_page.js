import React from 'react'
import withData from '../lib/withData'

const TestPage = () => {
    return <div>testpage</div>
}

export default withData(() => (<TestPage/>));
