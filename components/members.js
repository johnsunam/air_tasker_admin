import React from 'react'


const Memebers = () => {
    return <section className="info-slider">
    <div className="decorative-container"></div>
    <div className="container">
    {/* <Slider {...settings}> */}
        <div className="main-info-content">
            <div className="info-text col-md-6">
                <h2>Contrary to popular belief, Lorem Ipsum.</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                <div className="personal-info">
                    <img src="static/author.jpg" />
                    <span className="personal-info-text">
                        <span className="person-name">
                            Brandan Quick
                        </span>
                        <span className="position">
                            CEO and co-founder
                        </span>
                    </span>
                </div>
            </div>
            <div className="video-container col-md-6">
                <video controls>
                  <source src="static/mov_bbb.mp4" type="video/mp4" />
                  <source src="static/mov_bbb.ogg" type="video/ogg" />
                </video>
            </div>
        </div>
         <div className="main-info-content">
            <div className="info-text col-md-6">
                <h2>Contrary to popular belief, Lorem Ipsum.</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's</p>
                <div className="personal-info">
                    <img src="static/author.jpg"/>
                    <span className="personal-info-text">
                        <span className="person-name">
                            Brandan Quick
                        </span>
                        <span className="position">
                            CEO and co-founder
                        </span>
                    </span>
                </div>
            </div>
            <div className="video-container col-md-6">
                <video controls>
                  <source src="static/mov_bbb.mp4" type="video/mp4"/>
                  <source src="static/mov_bbb.ogg" type="video/ogg"/>
                </video>
            </div>
        </div>
        {/* </Slider> */}
    </div>
</section>
}

export default Memebers