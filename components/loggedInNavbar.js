import React, { Component } from 'react'
import Link from 'next/link'

import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

class LoggedInNavbar extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.handleLogout = this.handleLogout.bind(this)
  }

  handleLogout (e) {
    e.preventDefault()
    localStorage.clear()
    window.location.href = 'http://localhost:3001/'
  }

  
  render () {
    return( <header>
        <section className="main-header">
            <nav className="navbar navbar-default container">
                <div className="navbar-header col-md-3">
                    <div className="hamburger-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                  <a className="navbar-brand" href="#">
                    <img alt="Brand" src="../static/logo.png" />
                  </a>
                </div>
                <div className="nav-container col-md-9">
                    <div className="mid-nav col-md-8">
                        <ul className="mid-nav-list">
                            <li className="main-action"><Link href="/add-task"><a ><span className="plus"></span><span className="plus"></span><span className="text">Post a task</span></a></Link></li>
                            <li><a href="">Browse Tasks</a></li>
                            <li><a href="">My Tasks</a></li>
                            <li><a href="">My Offers</a></li>
                            <li><a href="">My Jobs</a></li>

                        </ul>
                    </div>
                    <div className="right-nav col-md-4">
                        <ul className="right-nav-list">
                            <li><a href="">Help</a></li>
                            <li><a className="notification-menu" href=""><img src="../static/bell.png" /></a></li>
                            <li><a className="notification-menu" href=""><img src="../static/mail.png" /><span className="notification">10</span></a></li>
                            <li className="account-menu"><a href="javascript:void(0)"><img className="user-image" src="../static/user.png" /></a>
                                <ul className="account-list">
                                    <li className="dummy-hover"></li>
                                    <li><a href=""><span className="name">Taju s.</span> Public Profile</a> </li>
                                    <li><a href="">Dasboard</a> </li>
                                    <li><a href="">Post a task</a> </li>
                                    <li><a href="">Browse tasks</a> </li>
                                    <li><a href="">My tasks</a> </li>
                                    <li><a href="">Messages</a> </li>
                                    <li><a href="">Payment history</a> </li>
                                    <li><a href="">Payment methods</a> </li>
                                    <li><a href="">Notification s</a> </li>
                                    <li><a href="">Refer a friend</a> </li>
                                    <li><a href="">Settings</a> </li>
                                    <li><a href="">Discover</a> </li>
                                    <li><a href="">Help topics</a> </li>
                                    <li><a href="" onClick={this.handleLogout}>Logout</a> </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </section>
    </header> )
  }
}

export default LoggedInNavbar
