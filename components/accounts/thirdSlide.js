import React,{Component} from 'react'
import {graphql, compose} from 'react-apollo'
import gql from 'graphql-tag'

class ThirdSlide extends Component {
    constructor (props) {
        super(props)
        this.updateUserDetail = this.updateUserDetail.bind(this)
    }

    updateUserDetail () {
        const mobile = this.refs.mobile.value
        this.props.updateUserDetail(this.props.id, mobile)
        .then(result => {
            console.log('result', result)
        })
        .catch(err => {
            console.log('err', err)
        })
    }

    render () {
        return <div>
            <div className="modal-header">
            <h4 className="modal-title">Mobile Number</h4>
          </div>
          <div className = "main-signup-content">
            <div className="mobile-number form-group">
              <label for="mobile-number">Mobile number</label>
              <span>We will send you a verification code</span>
              <input type="number" ref="mobile" placeholder="04123456786" className="form-control"/>
              <button className="btn btn-success btn-primary" onClick={this.updateUserDetail}>Send</button>
            </div>
            <p>Verifying your mobile number helps us know you’re a genuine human! We won’t show it to anyone or sell it on to any 3rd party, it’s just for us to send you some good stuff.</p>
          </div>
        </div>
    }
}

export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){_id}}`

export default compose(graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (_id, mobile) => 
        (mutate({
            variables: {data:{ _id, mobile}},
            update: (proxy, {data: {updateUserDetail}}) => {
                console.log('loginUser======>>>>>',updateUserDetail)
            }
        }))
    })
})
)(ThirdSlide)

