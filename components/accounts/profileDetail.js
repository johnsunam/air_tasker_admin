import React, { Component } from 'react'
import {graphql, compose} from 'react-apollo'
import gql from 'graphql-tag'
import FileReaderInput from 'react-file-reader-input'
import TagsInput from 'react-tagsinput'
 
import 'react-tagsinput/react-tagsinput.css'

class ProfileDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstEdit: null,
            secondEdit: null,
            thirdEdit: null,
            forthEdit: null,
            ...props.data.getUser
        }
        this.updateUserDetail = this.updateUserDetail.bind(this)
    }

    uploadImage () {

    }

    handleChange (e, results)  {
        let self = this
        results.forEach(result => {
          const [e, file] = result;
          console.log(e.target.result)
        //   self.props.updateUserDetail(this.props.id, e.target.result)
        //   .then(result => {
        //       self.setState({
        //           files: e.target.result
        //       })
        //       self.props.updateValues({image: e.target.result})
        //   })
        //   .catch(err => {
        //       console.log('errrrr', err)
        //   })
          console.log(`Successfully uploaded ${file.name}!`);
        });
    }

    updateUserDetail (data) {
        console.log('dataaaaaaaa', data)
        data._id = this.props.id
        this.props.updateUserDetail(data)
        .then(result => {
            console.log('rrrrrr', result)
            this.setState({
                firstEdit: null,
            secondEdit: null,
            thirdEdit: null,
            forthEdit: null,
                ...data
            })
        })
        .catch(err => {
            console.log('err', err)
        })

    }

    render () {
        console.log('=======>>>>>>>>', this.props)
        const { error, getUser } = this.props.data
        if(error) {
            return <div>Loading....</div>
        }
        if(getUser) {
            return <div className="main-body-wrapper col-md-12">
            <FileReaderInput  id="my-file-input"
                            onChange={this.handleChange}>
              <button ref="fileImage" style={{display: 'none'}}>Select a file!</button>
            </FileReaderInput>
        <img className="banner-edit"  src="../static/edit.png" onClick={()=> this.setState({firstEdit: true})}/>
        <div className="banner-image">
        <img src="../static/banner-image.jpg" />
        {this.state.firstEdit ? <div style={{marginBottom: 10}}>
            <div className="input-group col-md-8" style={{padding: 20}}>
            <input type="text" ref="firstName" value={this.state.firstName} onChange={e => this.setState({firstName: e.target.value})} className="form-control" placeholder="First Name"  />
            </div>

            <div className="input-group col-md-8" style={{padding: 20}}>
            <input type="text" ref="lastName" value={this.state.lastName} onChange={e => this.setState({lastName: e.target.value})} className="form-control" placeholder="Last name"  />
            </div>
            <div className="input-group col-md-8" style={{padding: 20}}>
            <button type="button" className="btn btn-success" style={{margin: 10}} onClick={() => {
                this.updateUserDetail({firstName: this.refs.firstName.value, lastName: this.refs.lastName.value})
            }}>Save</button>
            <button type="button" className="btn btn-default" style={{margin: 10}} onClick={()=> this.setState({firstEdit: false})}>Cancel</button>
            </div>
        </div>:''}
        <div className="main-profile-content-container col-md-12">
            <div className="col-md-4 profile-sidebar">
                {this.state.firstEdit ?"":<div className="col-md-12 profile-name-image">
                    <div className="sidebar-image-container">
                        <img src="../static/profile-image.jpg" />
                        <h4 className="user-name">{getUser.firstName} {getUser.lastName[0]}</h4>
                        <span className="online-status">Last online 52 mins ago</span>
                        <span className="location">Indigo Valley VIC, Australia</span>
                        <span className="member-history">Member since 6th Oct 2016</span>
                    </div>
                </div>}
                <div className="clearfix"></div>
                <div className="sidebar-info-container">
                    <div className="sidebar-sub-container">
                        <h4 className="badges">badges</h4>
                        <span className="badge-id">id badges</span>
                        <span className="mobile">Mobile</span>
                        <button>Learn More</button>
                    </div>
                    <div className="sidebar-sub-container">
                        <span className="badge-id">LICENCE BADGES</span>
                        <span className="license-desc">Show off your hard-earned qualifications and licences</span>
                        <button>Get a badge</button>
                    </div>
                    <div className="sidebar-sub-container">
                        <span className="badge-id">PArtnership BADGES</span>
                        <span className="license-desc">Show off your hard-earned qualifications and licences</span>
                        <button>Get a badge</button>
                    </div>
                </div>
            </div>
            <div className="col-md-8 main-profile-container">
                    <div className="main-profile-info-wrapper col-md-12">
                        <div className="col-md-6 preference-container">
                            <div className="preference-span-wrapper">
                                 <span className="as-tasker">
                                    As a Tasker
                                </span>
                                <span className="as-poster">
                                    As a Poster
                                </span>
                            </div>
                            <div className="preference-description">
                                <ul>
                                    <li><img src="../static/active-rating.png" /></li>
                                    <li><img src="../static/active-rating.png" /></li>
                                    <li><img src="../static/active-rating.png" /></li>
                                    <li><img src="../static/active-rating.png" /></li>
                                    <li><img src="../static/active-rating.png" /></li>
                                </ul>
                                <div className="rating">4.9 stars from 107 reviews</div>
                                <div className="completion">95% Completion rate</div>
                                <div className="completed-tasks">117 completed tasks</div>
                            </div>
                        </div>
                        <div className="col-md-12 description">
                        {this.state.secondEdit ? <div style={{marginBottom: 10}}>
                    <div className="input-group col-md-8" style={{padding: 20}}>
                        <textarea ref="description" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div className="input-group col-md-8" style={{padding: 20}}>
                    <button type="button" className="btn btn-success" style={{margin: 10}} onClick={() => {
                        this.updateUserDetail({description: this.refs.description.value})
                    }}>Save</button>
                    <button type="button" className="btn btn-default" style={{margin: 10}} onClick={()=> this.setState({secondEdit: false})}>Cancel</button>
                    </div>
                </div>:<div className="about">
                                <img className="edit" src="../static/edit.png" onClick={()=> this.setState({secondEdit: true})}/>
                                <h4>ABOUT</h4>
                                <p>{getUser.description}</p>
                                {/* <p>Our dedicated team of designers, developers and consultants are all about building your online presence – to make you and your business stand out from the crowd. We remain available 24/7 for our valued clients to develop & design as they demand. Client satisfaction is our achievement. Explore us and get amazed by our splendid services.</p> */}
                            </div>}
                            
                            <div className="about portfolio">
                                <h4>PORTFOLIO</h4>
                                <div className="portfolio-wrapper">
                                    <img className="edit" src="../static/edit.png" onClick={()=> this.setState({thirdEdit: true})}/>
                                    <ul className="portfolio-images">
                                        <li><img src="https://picsum.photos/200/300?image=1082" /></li>
                                        <li><img src="https://picsum.photos/200/300?image=1060" /></li>
                                        <li><img src="https://picsum.photos/200/300?image=1082" /></li>
                                        <li><img src="https://picsum.photos/200/300?image=1027" /></li>
                                        <li><img src="https://picsum.photos/200/300?image=1011" /></li>
                                    </ul>
                                </div>
                            </div>
                            {this.state.forthEdit ? <div style={{marginBottom: 10}}>
                                <div className="input-group col-md-8" style={{padding: 20}}>
                                <label>Education</label>
                                <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" className="form-control" data-role="tagsinput" />
                                </div>

                                <div className="input-group col-md-8" style={{padding: 20}}>
                                <input type="text" ref="lastName" value={this.state.lastName} onChange={e => this.setState({lastName: e.target.value})} className="form-control" placeholder="Last name"  />
                                </div>
                                <div className="input-group col-md-8" style={{padding: 20}}>
                                <button type="button" className="btn btn-success" style={{margin: 10}} onClick={() => {
                                    this.updateUserDetail({firstName: this.refs.firstName.value, lastName: this.refs.lastName.value})
                                }}>Save</button>
                                <button type="button" className="btn btn-default" style={{margin: 10}} onClick={()=> this.setState({forthEdit: false})}>Cancel</button>
                                </div>
                            </div>:<div className="about col-md-12">
                            <img className="edit" src="../static/edit.png" onClick={()=> this.setState({forthEdit: true})}/>
                                <h4>SKILLS</h4>
                                <div className="col-md-3 tag-container">
                                    <h5>education</h5>
                                    <span className="tags"> Business Graduate</span>
                                    <span className="tags">Digital Marketing Certificates</span>
                                </div>
                                <div className="col-md-6 tag-container">
                                    <h5>specialities</h5>
                                    <span className="tags">Graphic Design</span>
                                    <span className="tags">UI development</span>
                                    <span className="tags">Wordpress</span>
                                    <span className="tags">HTML</span>
                                    <span className="tags">PSD Design</span>
                                </div>
                                <div className="col-md-3 tag-container">
                                    <h5>language</h5>
                                    <span className="tags">English</span>
                                    <span className="tags">Hindi</span>
                                    <span className="tags">Urdu</span>
                                </div>
                                <div className="clearfix"></div>
                                <div className="col-md-3 tag-container">
                                    <h5>work</h5>
                                    <span className="tags">6 years of experience</span>
                                </div>
                                <div className="col-md-6 tag-container">
                                    <h5>transportation</h5>
                                    <span className="tags">online</span>
                                </div>
                            </div>}
                            
                            <div className="about col-md-12">
                                <h4>REVIEWS</h4>
                                <div className="preference-span-wrapper">
                                    <span className="as-tasker">
                                       As a Tasker
                                   </span>
                                   <span className="as-poster">
                                       As a Poster
                                   </span>
                                   <select name="select-relevant" id="">
                                       <option value="Most relevant">Most Relevant</option>
                                   </select>
                               </div>
                               <div className="review-container">
                                   <div className="col-md-6">
                                        <div className="review-image-container">
                                            <img src="../static/review-image.png" alt="" />
                                        </div>
                                        <div className="review-text-container">
                                            <ul>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                            </ul>
                                            <span className="time">8 hours ago</span>
                                            <span className="logo-info">Simple B/W Equine logo</span>
                                            <span className="review-desc"><span className="reviewer-name">Louise S. said </span>"Nice job"</span>
                                        </div>
                                   </div>
                                   <div className="col-md-6">
                                        <div className="review-image-container">
                                            <img src="../static/review-image.png" alt="" />
                                        </div>
                                        <div className="review-text-container">
                                            <ul>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                            </ul>
                                            <span className="time">8 hours ago</span>
                                            <span className="logo-info">Simple B/W Equine logo</span>
                                            <span className="review-desc"><span className="reviewer-name">Louise S. said </span>"Nice job"</span>
                                        </div>
                                   </div>
                                   <div className="col-md-6">
                                        <div className="review-image-container">
                                            <img src="../static/review-image.png" alt="" />
                                        </div>
                                        <div className="review-text-container">
                                            <ul>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                            </ul>
                                            <span className="time">8 hours ago</span>
                                            <span className="logo-info">Simple B/W Equine logo</span>
                                            <span className="review-desc"><span className="reviewer-name">Louise S. said </span>"Nice job"</span>
                                        </div>
                                   </div>
                                   <div className="col-md-6">
                                        <div className="review-image-container">
                                            <img src="../static/review-image.png" alt=""/>
                                        </div>
                                        <div className="review-text-container">
                                            <ul>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                                <li><img src="../static/active-rating.png" /></li>
                                            </ul>
                                            <span className="time">8 hours ago</span>
                                            <span className="logo-info">Simple B/W Equine logo</span>
                                            <span className="review-desc"><span className="reviewer-name">Louise S. said </span>"Nice job"</span>
                                        </div>
                                   </div>
                               </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div> 
        }
        return <div>Loading....</div>
        
    }
}


export const getUser = gql`
    query ($id: String!) {
        getUser(id: $id) { 
            _id,
            firstName,
            lastName,
            avatar,
            description
        }
    }
`
export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){
    _id,
    firstName,
    lastName,
    avatar,
    description
}}`


export default compose(graphql(getUser,{options: (props) => ({
    variables: {
        id: props.id
    },
    fetchPolicy: 'network-only'
})}),
graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (data) => 
        (mutate({
            variables: {data:data},
            update: (proxy, {data: {updateUserDetail}}) => {
                const data = proxy.readQuery({
                    query: getUser,
                    variables:{
                        id: updateUserDetail._id
                    }
                })
                proxy.writeQuery({
                    query: getUser,
                    variables: {
                        id: updateUserDetail._id
                    },
                    data: {getUser:updateUserDetail}
                })
            }
        }))
    })
})
)(ProfileDetail);