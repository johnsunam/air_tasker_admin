import React, {Component}  from 'react'

const SixthSlide = (props) => {
    return <div>
    <div className="modal-header">
    <h4 className="modal-title">You are good to go</h4>
  </div>
  <div className="main-signup-content">
    <span className="upload-text">Nice work getting set up - now you're free to browse tasks and make offers in your area to your heart's content!</span>
    <div className="final-image-holder">
      <img src={props.image}/>
    </div>
  </div>
</div>
}

export default SixthSlide