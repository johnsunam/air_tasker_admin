import React, { Component } from 'react'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router'


class LoginForm extends Component {
  constructor(props) {
      super(props)
    //   this.email = React.createRef()
    //   this.password = React.createRef()
    this.login = this.login.bind(this);
  }

  login(e) {
      let self = this;
      e.preventDefault();
      console.log('logged in ', this.refs.email.value, this.refs.password.value)
      this.props.loginUser(this.refs.email.value, this.refs.password.value)
        .then(result => {
            self.props.closeModal()
            localStorage.setItem("token", result.data.loginUser.token)
            if (self.props.addTask)
             Router.push('/add-task')
            else
             self.props.changeLoginStatus(true)
        })
        .catch(err => {
            console.log('errr', JSON.parse(JSON.stringify(err)))
        })

  }

  render () {
      return <form>
      <div className="form-group">
          <input placeholder="Email" ref={'email'} type="email" className="form-control"  />
      </div>
      <div className="form-group">
      <input placeholder="Password" ref={'password'} type="password" className="form-control" />
      </div>
      <div className="pw-confirmation">
          <div className="main-remember form-check">
              <input type="checkbox" className="form-check-input" id="exampleCheck1" />
              <label className="form-check-label" htmlFor="exampleCheck1">Remember me?</label>
          </div>
          <a href="">Forgot Passwords?</a>
      </div>

      <div className="form-group">
          <button className="btn btn-primary" onClick={this.login}>Log In</button>
      </div>
  </form>
  }
}

export const loginUser = gql`mutation ($data: LoginInput!) { loginUser( data: $data){user{_id email} token}}`

export default graphql(loginUser, {
    props: ({mutate}) => ({
        loginUser: (email, password) => 
        (mutate({
            variables: {data:{email, password}},
            update: (proxy, {data: {loginUser}}) => {
                console.log('loginUser======>>>>>',loginUser)
            }
        }))
    })
})(LoginForm)
