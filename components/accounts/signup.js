import React, { Component } from 'react'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

class SignupForm extends Component {
  constructor (props) {
    super(props)
    this.signup = this.signup.bind(this)
  }

  signup () {
    let self = this;
    // e.preventDefault()
    this.props.signupUser(this.refs.email.value, this.refs.password.value)
      .then(result=> {
          console.log('rerrrrr', result.data.signupUser)
          self.props.signupStep(2)
          this.props.addUser(result.data.signupUser)
      })
      .catch(err => {
          console.log('errrrr', err)
      })
  }

  render () {
    return <div className="modal-content">
    <div className="modal-header">
        <button type="button" ref='close' className="close" data-dismiss="modal" aria-label="Close"><span></span><span></span></button>
        <h4 className="modal-title" id="myModalLabel">Join Us</h4>
    </div>
    <div className="modal-body">
    <form>
    <div className="form-group">
        <input ref='email' placeholder="Email" type="email" className="form-control" />
    </div>
    <div className="form-group">
    <input ref='password' placeholder="Password" type="password" className="form-control" />
    </div>
    <div className="form-group">
        <button className="btn btn-primary" onClick={(e)=> {
            e.preventDefault()
            this.signup()
            }}>Join Now</button>
    </div>
</form>
        
        <div className="seperator">
            Or
        </div>
        
        <div className="social-media-login">
            <a className="facebook">
                <span className="icon-holder">
                    <img src="../static/facebook-login.png" alt="" />
                </span>
                <span>Continue with Facebook</span>
            </a>
            <a className="google">
                <span className="icon-holder">
                    <img src="../static/google-login.png" alt="" />
                </span>
                <span>Continue with Google</span>
            </a>
        </div>

        <div className="terms">
            By signing up, I agree to Company Name <a href="">Terms & Conditions</a>, and <a href="">Community Guidelines.</a> <a href="">Privacy Policy.</a>
        </div>

        <div className="account-confirmation">
            Already have an account? <a href="" onClick={(e) => {
                this.refs.close.click()
                e.preventDefault()
                this.props.openSignIn()
                }}>Log In</a>
        </div>
    </div>
  </div>
  }
}

export const signupUser = gql`mutation ($data: SignupInput!) { signupUser( data: $data){user{_id } token}}`

export default graphql(signupUser, {
    props: ({mutate}) => ({
        signupUser: (email, password) => 
        (mutate({
            variables: {data:{email, password}},
            update: (proxy, {data: {signupUser}}) => {
                console.log('loginUser======>>>>>',signupUser)
            }
        }))
    })
})(SignupForm)
