import React, {Component} from 'react'
import {graphql, compose} from 'react-apollo'
import gql from 'graphql-tag'

class ForthSlide extends Component {
    constructor (props) {
        super(props)
        this.updateUserDetail = this.updateUserDetail.bind(this)
    }

    updateUserDetail () {
        console.log(this.refs.description.value)
        this.props.updateUserDetail(this.props.id, this.refs.description.value)
        .then(result => {
            console.log('result', result)
        })
        .catch(err => {
            console.log('err', err)
        })
    }
    render () {
        return <div>
            <div className="modal-header">
              <h4 className="modal-title">Tell us more about you</h4>
          </div>
          <div className = "main-signup-content">
            <span className="upload-text">Enter a short description to help describe what you're experienced in and what you’d be good at.</span>
            <div className="form-group">
            <textarea ref="description" onChange={this.updateUserDetail} placeholder="Enter your short description about yourself" class="form-control" rows="8"></textarea>
          </div>
          </div>
        </div>
    }
}


export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){_id}}`

export default compose(graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (_id, description) => 
        (mutate({
            variables: {data:{ _id, description}},
            update: (proxy, {data: {updateUserDetail}}) => {
                console.log('loginUser======>>>>>',updateUserDetail)
            }
        }))
    })
})
)(ForthSlide)