import React, { Component } from 'react'
import {graphql, compose} from 'react-apollo';
import gql from 'graphql-tag';
import Select from 'react-select';
import { getAllCountries, getStatesOfCountry, getCitiesOfState } from 'country-state-city';
import _ from 'underscore'
const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
  ];
class SignupForm2 extends Component {
    constructor (props) {
        super(props)
        this.state = {
            countryId: null,
            stateId: null,
            data: []
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount ()  {
        console.log(getStatesOfCountry(153))
        let data = _.map(getAllCountries(), country => {
            return { value: country.id, label: country.name, type: 'country' }
        })
        this.setState({data: data})
    }

    handleChange (selectedOption) {
        let data = []
        this.setState({selectedOption})
        console.log(`Option selected:`, selectedOption);
        if (_.findWhere(selectedOption, {type: 'country'})) {

            if (_.findWhere(selectedOption, {type:'state'})) {
                if (_.findWhere(selectedOption, {type: 'city'})) {
                    console.log(`Option selected:`, selectedOption);
                } else {
                    data = _.map(getCitiesOfState(selectedOption[1]['value']), city => {
                        return { value: city.id, label: city.name , type: 'city'}
                    })
                }
            } else {
                data = _.map(getStatesOfCountry(selectedOption[0]['value']), state => {
                    return {value : state.id, label: state.name, type: 'state'}
                })
                let selectedOpt = _.reject(selectedOption, option => option.type === 'city')
                this.setState({selectedOption: selectedOpt})
            }
        } else {
            data = _.map(getAllCountries(), country => {
                return { value: country.id, label: country.name, type: 'country' }
            })
            this.setState({selectedOption:[]})
        }

        this.setState({data: data})
    }




    handleSubmit (e) {
      e.preventDefault()
      let self = this
    //   console.log(this.props._id, this.refs.firstName.value, this.refs.lastName.value, this.refs.done.checked, this.refs.earn.checked)
    console.log(this.props)
      let suburb = {}
      this.props.suburb(this.state.selectedOption)
      _.each(this.state.selectedOption, obj => {
          if (obj.type === 'country') {
              suburb.countryName = obj.label
              suburb.countryId = obj.value
          } else if (obj.type === 'state') {
              suburb.stateName = obj.label
              suburb.stateId = obj.value
          } else {
              suburb.cityName = obj.label
              suburb.cityId = obj.value
          }
      })

      this.props.updateUserDetail(this.props._id, this.refs.firstName.value, this.refs.lastName.value,JSON.stringify(suburb), this.refs.done.checked, this.refs.earn.checked)
      .then(result => {
          console.log('========>result', result, this.props.token)
         localStorage.setItem('token', this.props.token)
         localStorage.setItem('userId', this.props._id)
         self.props.signupStep(3)

        //  self.props.updateSuburb(this.state.selectedOption)
        //  self.refs.close.click()
        //  this.props.changeLoginStatus(true)

      })
      .catch(error => {
        console.log('errrorrrr', error)
      })

    }
    
    render () {
        return <div className="modal-content">
        <div className="modal-header">
            <button type="button" ref='close' className="close" data-dismiss="modal" aria-label="Close"><span></span><span></span></button>
            <h4 className="modal-title" id="myModalLabel">Welcome to Company Name</h4>
        </div>
        <div className="modal-body">
            <div className="profile-description-form-intro">
                    Tell us about yourself to set up your profile
            </div>
            <form action="">
                <div className="form-group">
                    <input ref='firstName' placeholder="First Name" type="text" className="form-control" id="firstName" />
                </div>
                <div className="form-group">
                    <input placeholder="Last Name" ref='lastName' type="text" className="form-control" id="lastName" />
                </div>
                {/* <div className="form-group">
                    <input placeholder="Enter your home suburb" ref='suburb' type="text" className="form-control" id="enterHome" />
                </div> */}
                <div className="form-group">
                     <Select
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={this.state.data}
                        isSearchable={true}
                        isMulti={true}
                    />
                </div>
                <div className="form-group signup-first">
                    <span className="signup-desc">What would you like to use Company Name for?</span>
                    <div className="form-check">
                        <input className="form-check-input" value="" type="checkbox" ref='done'/>
                        <label className="form-check-label" for="gridCheck1">
                        I want do get things done
                        </label>
                    </div><div className="form-check">
                        <input className="form-check-input" value="" type="checkbox" id="gridCheck1" ref='earn'/>
                        <label className="form-check-label" for="gridCheck1">
                        I want earn money
                        </label>
                    </div>
                    {/* <textarea type="text" placeholder="What would you like to use Company Name for?" className="md-textarea form-control" rows="3"></textarea> */}
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Continue</button>
                </div>
            </form>
        </div>
      </div>
    }
}
export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){_id, suburb}}`

export default compose(graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (_id, firstName, lastName, suburb, done, earn) => 
        (mutate({
            variables: {data:{ _id,firstName, lastName, suburb, done, earn }},
            update: (proxy, {data: {updateUserDetail}}) => {
                console.log('loginUser======>>>>>',updateUserDetail)
            }
        }))
    })
})
)(SignupForm2)

