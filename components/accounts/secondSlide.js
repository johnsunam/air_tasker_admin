import React, {Component} from 'react'
import {graphql, compose} from 'react-apollo'
import gql from 'graphql-tag'
import { getAllCountries, getStatesOfCountry, getCitiesOfState } from 'country-state-city'
import Select from 'react-select'
import _ from 'underscore'

class SecondSlide extends Component {
    constructor (props) {
        console.log('////////////////second', props)
        super(props)
        this.state ={
            selectedOption: props.selectedOption,
            alerts: []
        }
        this.handleChange = this.handleChange.bind(this)
        this.addTask = this.addTask.bind(this)

    }

    componentDidMount ()  {
        console.log(getStatesOfCountry(153))
        let data = _.map(getAllCountries(), country => {
            return { value: country.id, label: country.name, type: 'country' }
        })
        this.setState({data: data})
    }

    addTask () {

        let self = this
        const taskKind = this.refs.taskKind.value
        const range = this.refs.range.value

        let suburb = {}
        _.each(this.state.selectedOption, obj => {
            if (obj.type === 'country') {
                suburb.countryName = obj.label
                suburb.countryId = obj.value
            } else if (obj.type === 'state') {
                suburb.stateName = obj.label
                suburb.stateId = obj.value
            } else {
                suburb.cityName = obj.label
                suburb.cityId = obj.value
            }
        })

        let alert = `${taskKind} within ${range}km of ${suburb.countryName ? suburb.countryName:''} ${suburb.stateName ? suburb.stateName:''} ${suburb.cityName ? suburb.cityName:''} `
        console.log('////////', alert)
        this.props.addAlert(this.props.id, taskKind, parseInt(range), JSON.stringify(suburb))
        .then(result => {
            console.log('successs!!!!!!', result)
            let alerts = self.state.alerts
            alerts.push(alert)
            self.setState({alerts})
        })
        .catch(error => {
            console.log('error', error)
        })
    }


    handleChange (selectedOption) {
        let data = []
        this.setState({selectedOption})
        console.log(`Option selected:`, selectedOption);
        if (_.findWhere(selectedOption, {type: 'country'})) {

            if (_.findWhere(selectedOption, {type:'state'})) {
                if (_.findWhere(selectedOption, {type: 'city'})) {
                    console.log(`Option selected:`, selectedOption);
                } else {
                    data = _.map(getCitiesOfState(selectedOption[1]['value']), city => {
                        return { value: city.id, label: city.name , type: 'city'}
                    })
                }
            } else {
                data = _.map(getStatesOfCountry(selectedOption[0]['value']), state => {
                    return {value : state.id, label: state.name, type: 'state'}
                })
                let selectedOpt = _.reject(selectedOption, option => option.type === 'city')
                this.setState({selectedOption: selectedOpt})
            }
        } else {
            data = _.map(getAllCountries(), country => {
                return { value: country.id, label: country.name, type: 'country' }
            })
            this.setState({selectedOption:[]})
        }

        this.setState({data: data})
    }

    render () {
        console.log('/////////', this.state)
        return <div>
            <div className="modal-header">
            <h4 className="modal-title" id="myModalLabel">Let us know what your skills are</h4>
          </div>
          <div className = "main-signup-content phase-2">
            <span className="upload-text">When new tasks match your skills, we’ll alert you so you can be the first to make an offer.</span>
            {/* <form> */}
            <div className="form-group">
              <label for="work-type">What kind of tasks are you looking for?</label>
              <input type="text" ref ='taskKind' placeholder="eg. Pat For Rent" className="form-control" id="work-type"/>
                {/* <CategoryList /> */}
            </div>
            <div className="form-group">
              <div className="radio">
                  <label><input type="radio" name="optradio" />In Person</label>
              </div>
              <div className="radio">
                  <label><input type="radio" name="optradio" />Online</label>
              </div>
              <div className ="address-selector">
                  {/* <div className="form-group">
                    <input type="text" placeholder="Auburn NSW, Australia" className="form-control" id="work-type"/>
                  </div> */}
                  <div className="form-group">
                     <Select
                        value={this.state.selectedOption}
                        onChange={this.handleChange}
                        options={this.state.data}
                        isSearchable={true}
                        isMulti={true}
                    />
                </div>
                  <div className = "form-group kilometer">
                    <select class="form-control" ref="range">
                      <option value="10">10km +</option>
                      <option value="20">20km +</option>
                      <option value="30">30km +</option>
                      <option value="40">40km +</option>
                    </select>
                  </div>
              </div>
            </div>
            <button className = "btn btn-success" onClick ={this.addTask}>Add Task Alert</button>
            <a className = "alert-button">My Alerts</a>
            <div style={{marginBottom: 20}}>
                {this.state.alerts.map(alert => {
                    return <div>{alert}</div>
                })}
            </div>
            
            {/* </form> */}
          </div>
        </div>
    }
}


export const addAlert = gql`mutation ($data: AddAlertInput!) { addAlert( data: $data){_id}}`

export default compose(graphql(addAlert, {
    props: ({mutate}) => ({
        addAlert: (userId, taskKind, range, suburb) => 
        (mutate({
            variables: {data:{ userId, taskKind, range, suburb}},
            update: (proxy, {data: {addAlert}}) => {
                console.log('loginUser======>>>>>',addAlert)
            }
        }))
    })
}),
)(SecondSlide)