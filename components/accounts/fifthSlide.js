import React, {Component} from 'react'
import {graphql, compose} from 'react-apollo'
import gql from 'graphql-tag'

class FifthSlide  extends  Component {
    constructor (props) {
        super(props)
        this.updateUserDetail = this.updateUserDetail.bind(this)
    }

    updateUserDetail () {
        let self = this
        const accHolderName = this.refs.accHolderName.value
        const bsb = this.refs.bsb.value
        const  accNum = this.refs.accNum.value

        this.props.updateUserDetail(this.props.id, accHolderName, bsb, accNum)
        .then(result => {
            console.log('result', result)
            self.props.next()
        })
        .catch(err => {
            console.log('err', err)
        })
    }

    render () {
        return<div>
            <div className="modal-header">
            <h4 className="modal-title">Getting you paid</h4>
          </div>
          <div className="main-signup-content">
            <div className="form-group">
              <label for="account-holder-name">Account Holder Name</label>
              <input ref="accHolderName" type="text" className="form-control"/>
            </div>
            <div className="form-group">
              <label for="bsb">BSB</label>
              <input ref="bsb" type="text" className="form-control"/>
            </div>
            <div className="form-group">
              <label for="account-number">Account Number</label>
              <input ref="accNum" type="number" className="form-control"/>
            </div>
            <button className="btn btn-success btn-primary" onClick={this.updateUserDetail}>Add Bank Account</button>
          </div>
        </div>
    }
}

export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){_id}}`

export default compose(graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (_id, accHolderName, bsb, accNum) => 
        (mutate({
            variables: {data:{ _id, accHolderName, bsb, accNum}},
            update: (proxy, {data: {updateUserDetail}}) => {
                console.log('loginUser======>>>>>',updateUserDetail)
            }
        }))
    })
})
)(FifthSlide)