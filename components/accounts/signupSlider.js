import React, {Component} from 'react'
import Slider from 'react-slick'
import FirstSlide from './firstSlide'
import SecondSlide from './secondSlide'
import ThirdSlide from './thirdSlide'
import ForthSlide from './forthSlide'
import FifthSlide from './fifthSlide'
import SixthSlide from './sixthSlide'

class SignupSlider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      id: this.props._id,
      image: null,
      suburb: [],
      currentSlide: null
    }

    this.handleChange = this.handleChange.bind(this)
    this.next = this.next.bind(this)
    this.previous = this.previous.bind(this)
    this.updateValues = this.updateValues.bind(this)
  }
    componentDidMount() {
        document.getElementsByClassName('.modal-content .slick-next').innerHTML = "Next"
          console.log('slider checked')
    }

    next() {
      this.slider.slickNext();
    }
    previous() {
      this.slider.slickPrev();
    }

    updateValues (data) {
      if (data.image) {
        this.setState({image: data.image})
      } else if(data.suburb) {
        this.setState({suburb: data.suburb})
      }
    }

    handleChange (e, results)  {
      results.forEach(result => {
        const [e, file] = result;
        console.log(e.target.result)
        this.setState({files: e.target.result})
        console.log(`Successfully uploaded ${file.name}!`);
      });
    }


  

    render () {
      let self = this
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            beforeChange: function(currentSlide, nextSlide) {
              console.log("before change", currentSlide, nextSlide);
              if (currentSlide == 5 && nextSlide == 0) {
                  self.refs.close.click()
                  self.props.changeLoginStatus(true)
              }
            },
          }
          console.log('propssssss', this.props)
        return<div className="modal-content">
              <button type="button" ref="close" className="close" data-dismiss="modal" aria-label="Close"><span></span><span></span></button>
              <a className="skip">Skip</a>
        <Slider ref={c => (this.slider = c)} {...settings}>
        <div key={1}>
          <FirstSlide {...this.state} handleChange={this.handleChange} next={this.next} updateValues={this.updateValues}/>
        </div>
        <div key={2}>
          <SecondSlide {...this.state} {...this.props} next={this.next}/>
        </div>
        <div key={3}>
          <ThirdSlide {...this.state} next={this.next}/>
        </div>
        <div key={4}>
          <ForthSlide {...this.state} next={this.next}/>
        </div>
        <div key={5}>
          <FifthSlide {...this.state} next={this.next}/>
        </div>
        <div key={6}>
          <SixthSlide {...this.state}  next={this.next}/>
        </div>
      </Slider></div> 
    }
}

export default SignupSlider