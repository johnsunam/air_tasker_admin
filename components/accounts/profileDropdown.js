import React from 'react'
import Link from 'next/link'
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'

const ProfileDropdown = (props) => {
    const { error, getUser } = props.data
    console.log('///////', getUser)
    if(error)
     return <li>Loading....</li>
    if(getUser)
    return  <li class="account-menu"><a href="javascript:void(0)"><img class="user-image" src={getUser.avatar}/></a>
    <ul class="account-list">
        <li class="dummy-hover"></li>
        <li><Link href="/profile"><a ><span class="name">{getUser.firstName}.{getUser.lastName[0]}</span> Public Profile</a></Link> </li>
        <li><Link><a >Dasboard</a></Link> </li>
        <li><Link href="/add-task"><a >Post a task</a></Link> </li>
        <li><Link ><a >Browse tasks</a></Link> </li>
        <li><Link href="/my-task"><a >My tasks</a></Link> </li>
        <li><Link><a >Messages</a></Link> </li>
        <li><Link><a >Payment history</a></Link> </li>
        <li><Link><a >Payment methods</a></Link> </li>
        <li><Link><a >Notification s</a></Link> </li>
        <li><Link><a >Refer a friend</a></Link> </li>
        <li><Link><a >Settings</a></Link> </li>
        <li><Link><a >Discover</a></Link> </li>
        <li><Link><a >Help topics</a></Link> </li>
        <li><Link><a  onClick={props.handleLogout}>Logout</a></Link> </li>
    </ul>
</li>
   return  <li>Loading....</li>


}

export const getUser = gql`
    query ($id: String!) {
        getUser(id: $id) { 
            _id,
            firstName,
            lastName,
            avatar
        }
    }
`

export default graphql(getUser,{options: (props) => ({
    variables: {
        id: props.id
    },
})})(ProfileDropdown);