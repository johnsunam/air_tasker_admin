import React, {Component} from 'react'
import {graphql, compose} from 'react-apollo';
import gql from 'graphql-tag';
import FileReaderInput from 'react-file-reader-input'

class FirstSlide extends Component {
    constructor (props) {
        super(props)
        this.state = {
            files: null
        }

        this.handleChange = this.handleChange.bind(this)

    }

    handleChange (e, results)  {
        let self = this
        results.forEach(result => {
          const [e, file] = result;
          console.log(e.target.result)
          self.props.updateUserDetail(this.props.id, e.target.result)
          .then(result => {
              self.setState({
                  files: e.target.result
              })
              self.props.updateValues({image: e.target.result})
          })
          .catch(err => {
              console.log('errrrr', err)
          })
          console.log(`Successfully uploaded ${file.name}!`);
        });
    }


    render () {
        return <div>
        <div className="modal-header">
            <h4 className="modal-title" id="myModalLabel">Upload your profile picture</h4>
          </div>
          <div className = "main-signup-content">
            <span className="upload-text">Upload a clear photo of yourself to help people identify you from other members, and also gain trust.</span>
            <div className="user-image">
                {this.state.files ? <img src={this.state.files} alt=""/>:<img src='../../static/user-slider.jpg' alt=""/>}
            </div>
           

            <div className="upload-icon">
            <FileReaderInput  id="my-file-input"
                            onChange={this.handleChange}>
              <button ref="fileImage" style={{display: 'none'}}>Select a file!</button>
            </FileReaderInput>
               <img src="../../static/upload-icon.png" alt="" onClick={ev => {
                 this.refs.fileImage.click()
               }}/>
                
            </div>
          </div>
    </div>
    }
}

export const updateUserDetail = gql`mutation ($data: UpdateUserDetailInput!) { updateUserDetail( data: $data){_id, avatar}}`

export default compose(graphql(updateUserDetail, {
    props: ({mutate}) => ({
        updateUserDetail: (_id, avatar) => 
        (mutate({
            variables: {data:{ _id, avatar }},
            update: (proxy, {data: {updateUserDetail}}) => {
                console.log('loginUser======>>>>>',updateUserDetail)
            }
        }))
    })
})
)(FirstSlide)