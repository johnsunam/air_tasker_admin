import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import ProfileDropdown from '../components/accounts/profileDropdown'


class Navbar extends Component {
    constructor (props) {
        super(props)
        this.state = {
            userId: null
        }
    }
    handleLogout (e) {
        e.preventDefault()
        localStorage.clear()
        // window.location.href = 'http://localhost:3000/'
        Router.push({pathname: '/'})
    }

    componentDidMount () {
        this.setState({userId: localStorage.getItem('userId')})
    }
    
    render() { 
        return ( 
            <React.Fragment>
            <header>
                <section class="main-header">
                    <nav class="navbar navbar-default container">
                        <div class="navbar-header col-md-3">
                            <div class="hamburger-menu">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <a class="navbar-brand" href="#">
                                <img alt="Brand" src="../static/blue-logo.png" />
                            </a>
                        </div>
                        <div class="nav-container col-md-9">
                            <div class="mid-nav col-md-8">
                                <ul class="mid-nav-list">
                                    <li class="main-action"><Link href="/add-task"><a ><span class="plus"></span><span class="plus"></span><span class="text">Post a task</span></a></Link></li>
                                    <li><a href="">Browse Tasks</a></li>
                                    <li><Link href="/my-task"><a >My Tasks</a></Link></li>
                                    <li><a href="">My Offers</a></li>
                                    <li><a href="">My Jobs</a></li>
                                </ul>
                            </div>
                            <div class="right-nav col-md-4">
                                <ul class="right-nav-list">
                                    <li><a href="">Help</a></li>
                                    <li><a class="notification-menu" href=""><img src={this.props.home ? "../static/bell.png":"../static/profile-bell.png"} /></a></li>
                                    <li><a class="notification-menu" href=""><img src={this.props.home ? "../static/mail.png":"../static/mail-profile.png"}/><span class="notification">10</span></a></li>
                                    {this.state.userId ? <ProfileDropdown id={this.state.userId} handleLogout={this.handleLogout}/>:<li></li>}
                                </ul>
                            </div>
                        </div>
                    </nav>
                </section>
            </header>
            <div className="clearfix"></div>
            </React.Fragment>
         );
    }
}
 
export default Navbar