import React from 'react'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';


const OptionList = (props) => {
    const {error, getCategories} = props.data
    console.log('/////////////',getCategories)
    if (error) {
        return <div>Loading....</div>
    }
    if (getCategories) {
        return getCategories.map((category, i) => <div key={i} className="radio">
        <label><input type="radio" name="category" onClick={props.changeCategory}  value={category._id}/>{category.title}</label>
    </div>)
    }
    return <div>hellloooo</div>
}

export const getCategories = gql`
    query {
        getCategories { 
            _id,
            title,
            parent
        }
    }
`

export default graphql(getCategories)(OptionList);