import React, { Component } from 'react'
import OptionList from './optionList'
import moment from 'moment'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router'
import {getTasks} from '../task/tasks-type'

class AddTaskerForm extends Component {
    constructor (props) {
        super(props)
        this.state  = {
            due_type: null,
            due_date: null,
            budget_type: null,
            budget: null,
            category:null
        }

        this.changeDueType = this.changeDueType.bind(this)
        this.changeBudgetType = this.changeBudgetType.bind(this)
        this.addTask = this.addTask.bind(this)
        this.changeCategory = this.changeCategory.bind(this)
    }
    changeBudgetType (e) {
        console.log('budget type', e.target.value)
        this.setState({budget_type: e.target.value})
    }

    changeDueType (e) {
        console.log('due type', e.target.value)
        if (e.target.value == 'today') {
            this.setState({due_date: moment().format('YYYY-MM-DD')})
        } else if (e.target.value == 'week') {
            this.setState({due_date: moment().add(7, 'days').format('YYYY-MM-DD')})
        }
        this.setState({due_type: e.target.value})

    }

    changeCategory (e) {
        this.setState({category: e.target.value})
    }

    addTask (e) {
        console.log('add tasks')
        let userId  = localStorage.getItem('userId')
        console.log(this.state.category, 
            this.refs.title.value, 
            this.refs.description.value, 
            this.state.due_date, 
            this.state.due_type, 
            this.state.budget,
            this.state.budget_type,
            this.refs.tasker_no.value,
            userId)
        this.props.addTask(
            this.state.category, 
            this.refs.title.value, 
            this.refs.description.value, 
            this.state.due_date, 
            this.state.due_type, 
            parseInt(this.state.budget),
            this.state.budget_type,
            parseInt(this.refs.tasker_no.value),
            userId
            )
        .then(result  => {
            Router.push({pathname:'/my-task'})
            console.log('results', result)
        })
        .catch(err => {
            console.log('errr', err)
        })

    }

    render () {
        return <main className = "container add-tasker">
        <h3>Get FREE Quotes</h3>
        <div className = "main-option-container">
            <div className = "option-container">
                <h4>Options to select<span className="info-image-wrapper"><img src="../static/info.png"/></span></h4>
                <OptionList changeCategory={this.changeCategory}/>
            </div>
        </div>
    
        <div className ="main-tasker-input-wrapper">
            <div className = "tasker-input-wrapper">
            <h4>TASK DETAIL</h4>
            <div className="form-group">
                    <label>Task Title<span className="info-image-wrapper"><img src="../static/info.png"/></span></label>
                    <input ref="title" type="text" className="form-control" aria-describedby="emailHelp" placeholder="Post a Task" />
                    <span className ="confirmation-image-holder"><img src="../static/confirmation.png"/></span>
                </div>
                <div className="form-group">
                    <label>Describe your task in more detail<span className="info-image-wrapper"><img src="../static/info.png"/></span></label>
                    <textarea ref="description" className="form-control"  placeholder="eg. Animal For Breeding" />
                    <span className ="confirmation-image-holder"><img src="../static/confirmation.png"></img></span>
                </div>
            </div>
        </div>
    
        <div className = "date-wrapper">
            <h4>Due date</h4>
            <div className="radio radio1">
                <label><input type="radio" onChange={this.changeDueType} name="due_type" value="today" />Today</label>
            </div>
            <div className="radio radio-2">
                <label><input type="radio" onChange={this.changeDueType} name="due_type"  value="certain"/>By a certain day</label>
            </div>
    
            <div className="radio">
                <label><input type="radio" onChange={this.changeDueType} name="due_type" value="week"/>Within 1 week</label>
            </div>
            <div className="form-group">
                <input type="date" ref="due_date" value={this.state.due_date} onChange={e => this.setState({due_date: e.target.value})}  className="form-control" aria-describedby="emailHelp" placeholder="5/12/18" />
                <span className ="confirmation-image-holder"><img src="../static/confirmation.png"/></span>
            </div>
        </div>
    
        <div className="budget-container col-md-12">
            <h4>budget<span className="info-image-wrapper"><img src="../static/info.png"/></span></h4>
            <div className="col-md-6 budget-confirmation">
               <div className = "budget-container">
               <label>Whats your budget?<span className="info-image-wrapper"><img src="../static/info.png"/></span></label>
                <div className="clearfix"></div>
                <div className="radio radio1">
                    <label><input type="radio" value="total" name="budget_type" onChange={this.changeBudgetType}  />Total</label>
                </div>
                <div className="radio radio-2">
                    <label><input type="radio" value="hour" name="budget_type" onChange={this.changeBudgetType} />Hourly Rate</label>
                </div>
                <div className="form-group">
                    <input type="number" ref="budget" onChange={e => this.setState({budget: e.target.value})} className="form-control" placeholder="eg.25" /><span className="value">$</span>
                    <span className ="confirmation-image-holder"><img src="../static/confirmation.png"/></span>
                </div>
               </div>
            </div>
            <div className="col-md-6 number-of-tasker">
                <div className = "number-of-tasker-container">
                <label>How many people do you need for your task?<span className="info-image-wrapper"><img src="../static/info.png"/></span></label>
                <div className="form-group">
                    <input type="number" ref="tasker_no" className="form-control" placeholder="eg.1" /><span className="value">Tasker</span>
                    <span className ="confirmation-image-holder"><img src="../static/confirmation.png"/></span>
                </div>
                </div>
            </div>
            <div className ="col-md-12 budget-calculation">
                <h3>Estimated Budget: ${this.state.budget}</h3>
                <h4>Whats happens next?<span className="info-image-wrapper"><img src="../static/info.png"/></span></h4>
            </div>
        </div>
        <button className ="btn btn-primary" onClick={this.addTask}>Get Quotes Now</button>
    </main>
    }
}


export const addTask = gql`mutation ($data: TaskInput!) { addTask( data: $data){_id, category, title, description, due_date, due_type, budget, budget_type, taskerNum, userId}}`

export default graphql(addTask, {
    props: ({mutate}) => ({
        addTask: (category, title, description, due_date, due_type, budget, budget_type, taskerNum, userId) => {
            console.log('in mutation',category, title, description, due_date, due_type, budget, budget_type, taskerNum, userId)
        return (mutate({
            variables: {data:{category, title, description, due_date, due_type, budget, budget_type, taskerNum, userId}},
            update: (proxy, {data: {addTask}}) => {
                console.log('loginUser======>>>>>',addTask)
                // const data = proxy.readQuery({
                //     query: getTasks
                //   });

                // proxy.writeQuery({
                //     query: getTasks,
                //     data: { 
                //         ...data,
                //         getTasks: [...data.getTasks, addTask]
                //     }
                // })
            }
        }))}
    })
})(AddTaskerForm)