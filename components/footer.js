import React, { Component } from 'react';

class Footer extends Component {
    render() { 
        return ( 
            <footer className="main-footer">
        <div className="container footer-container">
            <div className="col-md-2 col-sm-3">
                <h4>Discover</h4>
                <ul>    
                    <li><a href="">Airtasker cards</a></li>
                    <li><a href="">How it works</a></li>
                    <li><a href="">Airtasker for business</a></li>
                    <li><a href="">Earn Money</a></li>
                    <li><a href="">New user FAQ</a></li>
                </ul>
            </div>
            <div className="col-md-2 col-sm-3">
                <h4>Company</h4>
                <ul>    
                    <li><a href="">About us</a></li>
                    <li><a href="">Careers</a></li>
                    <li><a href="">Community guidelines</a></li>
                    <li><a href="">Terms & conditions</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">Contact us</a></li>
                    <li><a href="">Privacy policy</a></li>
                </ul>
            </div>
            <div className="col-md-2 col-sm-3">
                <h4>Existing Members</h4>
                <ul>    
                    <li><a href="">Post a task</a></li>
                    <li><a href="">Browse tasks</a></li>
                    <li><a href="">Support centre</a></li>
                    <li><a href="">Merchandise</a></li>
                </ul>
            </div>
            <div className="col-md-3 col-sm-3">
                <h4>Popular Categories</h4>
                <ul>    
                    <li><a href="">Handyman Services</a></li>
                    <li><a href="">Cleaning Services</a></li>
                    <li><a href="">Delivery Services</a></li>
                    <li><a href="">Removalists</a></li>
                    <li><a href="">Gardening Services</a></li>
                    <li><a href="">Food Delivery</a></li>
                    <li><a href="">Assembly Services</a></li>
                </ul>
            </div>
            <div className="col-md-3 social-media-holder col-sm-12">
                <img className="footer-logo" src="../static/logo.png" />
                <h4>Follow us :</h4>
                <ul className="social-media">
                    <li><a href=""><img src="../static/facebook.png" /></a></li>
                    <li><a href=""><img src="../static/twitter.png" /></a></li>
                    <li><a href=""><img src="../static/linkedin.png" /></a></li>
                    <li><a href=""><img src="../static/pinterest.png" /></a></li>
                    <li><a href=""><img src="../static/google-plus.png" /></a></li>
                </ul>
            </div>
        </div>
        <div className="footer-outro container">
            <p>Company name. Ltd 2011-2018 ©, All rights reserved</p>
        </div>  
    </footer>
         );
    }
}
 
export default Footer;