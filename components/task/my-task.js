import React, { Component } from 'react'
import TasksType from './tasks-type'

class MyTask extends Component {
    constructor (props) {
        super(props)
        this.state = {
            type: 1,
            userId: null
        }
    }

    componentDidMount ( ) {
        console.log('my tasksssss')
        this.setState({userId: localStorage.getItem('userId')})
    }
    render () {
        const Sidebar = {1: 'All Tasks', 2: 'Posted Tasks', 3: 'Draft Tasks', 4: 'Task Assigned', 5: 'Offers Pending', 6: 'Tasks Completed'}
        return <main className="container">
        <div className ="tasker-container col-md-12">
            <div className= "col-md-3 tasker-sidebar">
                <div className="form-group">
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Search For A Task" />
                    <button className="btn btn-primary"></button>
                </div>
                <ul className="tasker-side-menu">
                    <li className={this.state.type == 1 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 1})}>all tasks</li>
                    <li className={this.state.type == 2 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 2})}>posted tasks</li>
                    <li className={this.state.type == 3 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 3})}>draft tasks</li>
                    <li className={this.state.type == 4 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 4})}>task assigned</li>
                    <li className={this.state.type == 5 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 5})}>offers pending</li>
                    <li className={this.state.type == 6 ? 'activeSidebar': ''} onClick={()=> this.setState({type: 6})}>tasks completed</li>
                </ul>
            </div>
            <div className ="col-md-9 tasker-main">
                <h3>{Sidebar[this.state.type]}</h3>
                {this.state.userId ? <ul className="tasker-details col-md-12">
                    <TasksType taskType = {this.state.type}/>
                    {/* <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li>
                    <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li>
                    <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li>
                    <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li>
                    <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li>
                    <li class="col-md-12">
                        <span className="task-purpose">Animal For Breeding</span>
                        <span className="tasker-wrapper">
                            <span className="task_row col-md-3 task_row_first">
                                <span className="title">
                                    Location
                                </span>
                                <span>
                                    Remote
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_second">
                                <span className="title">
                                    time
                                </span>
                                <span>
                                    Morning (before 10 am)
                                </span>
                            </span>
                            <span className="task_row col-md-3 task_row_third">
                                <span className="title">
                                       expire on
                                </span>
                                <span>
                                   Thursday, 13th Dec 2018
                                </span>
                            </span>
                            <span className="col-md-1 task_row task_row_fourth">
                                <img src="../static/user.png"/>
                            </span>
                            <span className="task_row col-md-2 task_row_fifth">
                                <span className="badge badge-sm">
                                      open
                                </span>
                                <span className = "task-price">
                                   $120
                                </span>
                            </span>
                        </span>
                    </li> */}
                    
                </ul>: <div>Loading ......</div>}

                
            </div> 
        </div>
        </main>
    }
}

export default MyTask