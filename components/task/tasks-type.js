import React from 'react'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import moment from 'moment';

const TasksType = (props) => {
    console.log('props', props)
    const { error, getTasks } = props.data
    console.log('getTasks ',getTasks)
    if(error) {
        return <li>Loading......</li>
    }
    if (getTasks)
    return getTasks.map((task, i) => {
        return <li key={i} class="col-md-12">
        <span className="task-purpose">{task.title}</span>
        <span className="tasker-wrapper">
            <span className="task_row col-md-3 task_row_first">
                <span className="title">
                    Location
                </span>
                <span>
                    Remote
                </span>
            </span>
            <span className="task_row col-md-3 task_row_second">
                <span className="title">
                    time
                </span>
                <span>
                    before {moment(Date(task.due_date)).format('hh:mma')}
                </span>
            </span>
            <span className="task_row col-md-3 task_row_third">
                <span className="title">
                       expire on
                </span>
                <span>
                   {moment(task.due_date).format('dddd, Do MMM YYYY')}
                </span>
            </span>
            <span className="col-md-1 task_row task_row_fourth">
                <img src={task.user.avatar}/>
            </span>
            <span className="task_row col-md-2 task_row_fifth">
                <span className="badge badge-sm">
                      open
                </span>
                <span className = "task-price">
                   ${task.budget}
                </span>
            </span>
        </span>
    </li>
    })
    return <li>Loading......</li>
}

export const getTasks = gql`
    query ($userId: String!) {
        getTasks(userId: $userId) { 
            _id,
            title,
            due_date,
            budget,
            user { _id avatar}
        }
    }
`

export default graphql(getTasks,{options: (props) => ({
    variables: {
        userId: localStorage.getItem('userId')
    },
    fetchPolicy: 'network-only'
})})(TasksType);