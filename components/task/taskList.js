import React, { Component } from 'react'

import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

import Link from 'next/link'

class TaskList extends Component {
    constructor (props) {
        super(props)
        this.state = {
            tasks: props.data.getTasks
        }
    }
    render () {
        const {error, getTasks} = this.props.data
        if(error)
        return <div>Loading...</div>
        if(getTasks)
        return this.state.tasks.map((task, i) => <Link><div className="main-offers-wrapper col-md-12" key={task._id}>
        <div className="col-md-3">
            <img src="../static/slider1.jpg"/>
        </div>
        <div className="col-md-8">
            <h5>{task.title}</h5>
            <div className="main-content">
                <img src={task.user.avatar}/>
                <p>{task.description}</p>
            </div>
            <div className="bottom-content">
                <img src="../static/rating.png" />
                <span className="rate">${task.budget}</span>
            </div>
        </div>
    </div></Link>)
    return <div>Loading...</div>
    }
}

export const getTasks = gql`
    query ($category: String!) {
        getTasks(category: $category) { 
            _id,
            title,
            description,
            budget,
            user { _id avatar}
        }
    }
`

export default graphql(getTasks,{options: (props) => ({
    variables: {
        category: props.category
    },
})})(TaskList);