import React, { Component } from 'react'
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

import TaskList from './task/taskList'

const TaskCategory = ({data}) => {
    const { error, getCategories } =  data
    console.log('/////////////', data)

    if (error) {
        return <section className="offers-sliders col-md-12">...loading</section>
    }
    if (getCategories) {
        return <section className="offers-sliders col-md-12">
        <h2>Latest Projects</h2>
        {getCategories.map((category, i) => <div key={i}><h4>{category.title}:</h4>
        <div className="slider-container col-md-12">
            <TaskList category={category._id}/>
            {/* <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg"/>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div> */}
        </div>
        <div className="clearfix"></div>
        </div>)}

        {/* <h4>Cats:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg"/>
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg"/>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg"/>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>
      
        <h4>For Breeding:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>
      
        <h4>Rats:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div>
      
        <h4>Rabbits:</h4>
        <div className="slider-container col-md-12">
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
            <div className="main-offers-wrapper col-md-12">
                <div className="col-md-3">
                    <img src="../static/slider1.jpg" />
                </div>
                <div className="col-md-8">
                    <h5>pat for rent</h5>
                    <div className="main-content">
                        <img src="static/author.jpg" />
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                    <div className="bottom-content">
                        <img src="../static/rating.png" />
                        <span className="rate">$139</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="clearfix"></div> */}
      </section>
    }
    return <section><div>Loading......</div></section>
}

export const getCategory = gql`
    query {
        getCategories { 
            _id,
            title 
        }
    }
`

export default graphql(getCategory)(TaskCategory);
