import React, { Component } from 'react';
import Link from 'next/link'
import LoginForm from '../components/accounts/login'
import SignupForm1 from '../components/accounts/signup'
import SignupForm2 from '../components/accounts/signup2'
import AddTaskForm from '../components/tasker/addTaskForm'
import SignupSlider from '../components/accounts/signupSlider'

class Navbar extends Component {
    constructor (props) {
      super(props)
      this.closeModal = this.closeModal.bind(this)
      this.signupStep = this.signupStep.bind(this)
      this.addUser = this.addUser.bind(this)
      this.openSignIn = this.openSignIn.bind(this)
      this.openSignup = this.openSignup.bind(this)
      this.suburb = this.suburb.bind(this)
      this.state = {
          step: 1,
          _id: null,
          addTask: false,
          selectedOption: []
      }
    }
    closeModal () {
        this.refs.close.click()
    }

    signupStep (step) {
        this.setState({step: step})
    }

    addUser (result) {
        console.log('user added', result)
        this.setState({_id: result.user._id, token: result.token})
    }

    suburb (selectedOption) {
        console.log('selected options', selectedOption)
        this.setState({selectedOption})
    }

    
    openSignup (e) {
        e.preventDefault()
        this.closeModal()
        this.refs.signupButton.click()
    }

    openSignIn () {
        this.closeModal()
        this.refs.loginButton.click()
    }

    render() { 
        return ( 
            <header>
        <section className="main-header">
            <nav className="navbar navbar-default container">
                <div className="navbar-header col-md-3">
                    <div className="hamburger-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                  <a className="navbar-brand" href="#">
                    <img alt="Brand" src="../static/logo.png" />
                  </a>
                </div>
                <div className="nav-container col-md-9">
                    <div className="mid-nav col-md-8">
                        <ul className="mid-nav-list">
                            <li className="main-action"><a data-toggle="modal" onClick={() => {
                                this.setState({addTask: true})
                                $('.login-bs-example-modal-lg').modal('toggle');
                            }}><span className="plus"></span><span className="plus"></span><span className="text">Post a task</span></a></li>
                            <li><a >Explore</a></li>
                            <li><a >Browse</a></li>
                            <li><a >How it works</a></li>
                        </ul>
                    </div>
                    <div className="right-nav col-md-4">
                        <ul className="right-nav-list">
                            <li><a >Help</a></li>
                            <li><a  data-toggle="modal" data-target=".login-bs-example-modal-lg" ref="loginButton">Login </a></li>
                            <li><a data-toggle="modal" data-target=".signup-bs-example-modal-lg" ref="signupButton">SignUp</a></li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </section>
        {/* login modal */}
        <div className="modal fade login-bs-example-modal-lg" tabIndex="-1"  role="dialog" aria-labelledby="myLargeModalLabel">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                    <button ref='close' type="button" className="close" data-dismiss="modal" aria-label="Close"><span></span><span></span></button>
                    <h4 className="modal-title" id="myModalLabel">Log In</h4>
                </div>
                <div className="modal-body">
                    <LoginForm {...this.props} closeModal={this.closeModal} addTask={this.state.addTask}/>
                    <div className="seperator">
                        Or
                    </div>
                    
                    <div className="social-media-login">
                        <a className="facebook">
                            <span className="icon-holder">
                                <img src="../static/facebook-login.png" alt="" />
                            </span>
                            <span>Continue with Facebook</span>
                        </a>
                        <a className="google">
                            <span className="icon-holder">
                                <img src="../static/google-login.png" alt="" />
                            </span>
                            <span>Continue with Google</span>
                        </a>
                    </div>

                    {/* <!-- <div className="terms">
                        By signing up, I agree to Company Name <a href="">Terms & Conditions</a>, and <a href="">Community Guidelines.</a> <a href="">Privacy Policy.</a>
                    </div> --> */}

                    <div className="account-confirmation">
                        Dont't have an account? <a href="" onClick={this.openSignup}>Sign Up</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        {/* signup modal */}
          <div className="modal fade signup-bs-example-modal-lg" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div className="modal-dialog modal-lg" role="document">
            {/* <SignupForm2 {...this.state} {...this.props} signupStep={this.signupStep}/> */}
            {/* <SignupSlider {...this.state}/> */}
            {this.state.step === 1 ?<SignupForm1 signupStep={this.signupStep} {...this.props} addUser={this.addUser} openSignIn={this.openSignIn}/>:''}
            {this.state.step === 2 ? <SignupForm2 {...this.state} {...this.props} signupStep={this.signupStep} suburb={this.suburb}/>:''}
            {this.state.step === 3 ? <SignupSlider {...this.state} {...this.props}/>:''}
            </div>
          </div>
        {/* add task modal */}
        <div className="modal fade addtask-bs-example-modal-lg" tabIndex="-1"  role="dialog" aria-labelledby="myLargeModalLabel">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-body"><AddTaskForm /></div>
              </div>
            </div>
          </div>
    </header>  
         );
    }
}
 
export default Navbar;