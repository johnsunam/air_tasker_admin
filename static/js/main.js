$(document).ready(function() {
	adjustPositionOfBannerText();
	addSliderToSecondSection();
	addResponsesToNavbar();
});

$(window).resize(function(){
	adjustPositionOfBannerText();
});


function adjustPositionOfBannerText() {
	var marginLeft = $('.container').css('margin-left');
	$('.banner-text').css('left', marginLeft);
}

function addSliderToSecondSection() {
	$('.info-slider .container').slick({

	});
	
	var sliderControl = $('<img src="static/arrow.png">');
	$('.slick-next, .slick-prev').append($(sliderControl));

	$('.slider-container').slick({
		centerMode: true,
	  infinite: true,
	  centerPadding: '90px',
	  slidesToShow: 3,
	  slidesToScroll : 1,
	  autoplay: true,
	  speed: 2000,
	  responsive: [
    {
      breakpoint: 1300,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false
      },
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: false
      }


    }
    ]
	});

	var secondSliderControl = $('<img src="static/slider.png">');
	$('.slider-container .slick-next, .slider-container .slick-prev').append($(secondSliderControl));

}

function addResponsesToNavbar() {
	$('.hamburger-menu').on('click',function(){
		console.log('hey');
		$(this).toggleClass('hamburger-menu-visible');
		$('.nav-container').toggleClass('nav-container-visible')
		$('.navbar-brand').toggleClass('navbar-brand-hidden');
		$('.main-action').toggleClass('main-action-visible');
	});
}